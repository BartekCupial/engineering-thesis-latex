\babel@toc {polish}{}
\babel@toc {polish}{}
\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{1}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Analiza problemu}{3}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Wst\IeC {\k e}p teoretczny}{3}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Sieci neuronowe}{3}{subsection.2.1.1}% 
\contentsline {subsubsection}{Propagacja wsteczna}{3}{subsubsection*.1}% 
\contentsline {subsubsection}{Funkcja aktywacji}{4}{subsubsection*.2}% 
\contentsline {subsubsection}{Przetrenowanie}{4}{subsubsection*.3}% 
\contentsline {subsubsection}{Funkcja straty}{5}{subsubsection*.4}% 
\contentsline {subsubsection}{Optymalizator}{5}{subsubsection*.5}% 
\contentsline {subsubsection}{Przeniesienie uczenia (transfer learning)}{5}{subsubsection*.6}% 
\contentsline {subsection}{\numberline {2.1.2}Konwolucyjne sieci neuronowe}{5}{subsection.2.1.2}% 
\contentsline {section}{\numberline {2.2}Dotychczasowe rozwi\IeC {\k a}zania}{6}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Tradycyjne podej\IeC {\'s}cia}{6}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Podej\IeC {\'s}cia zwi\IeC {\k a}zane z g\IeC {\l }\IeC {\k e}bokim uczeniem}{7}{subsection.2.2.2}% 
\contentsline {section}{\numberline {2.3}Przedmiot pracy}{7}{section.2.3}% 
\contentsline {chapter}{\numberline {3}Opis wykorzystywanych metod}{9}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Architektura sieci neuronowej}{9}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}ResNet}{9}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}DenseNet}{9}{subsection.3.1.2}% 
\contentsline {subsection}{\numberline {3.1.3}Funkcja aktywacji - Mish}{10}{subsection.3.1.3}% 
\contentsline {section}{\numberline {3.2}Selektywna wsteczna propagacja}{11}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Operacje wykonywane na danych}{11}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Zbi\IeC {\'o}r danych}{11}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Modyfikacje na zbiorze treningowym}{11}{subsection.3.3.2}% 
\contentsline {section}{\numberline {3.4}Polityka jednego cyklu}{11}{section.3.4}% 
\contentsline {chapter}{\numberline {4}Projekt systemu i implementacja}{13}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Projekt systemu}{13}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Za\IeC {\l }o\IeC {\.z}enia i architektura systemu}{13}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}Klient}{13}{subsection.4.1.2}% 
\contentsline {subsection}{\numberline {4.1.3}Serwer}{13}{subsection.4.1.3}% 
\contentsline {section}{\numberline {4.2}Implementacja systemu}{14}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Klient}{14}{subsection.4.2.1}% 
\contentsline {subsection}{\numberline {4.2.2}Serwer}{14}{subsection.4.2.2}% 
\contentsline {section}{\numberline {4.3}Trening sieci neuronowej}{14}{section.4.3}% 
\contentsline {subsection}{\numberline {4.3.1}Inicjalizacja i zamra\IeC {\.z}anie}{15}{subsection.4.3.1}% 
\contentsline {subsection}{\numberline {4.3.2}Wyb\IeC {\'o}r hiperparametr\IeC {\'o}w}{15}{subsection.4.3.2}% 
\contentsline {subsection}{\numberline {4.3.3}Trening}{16}{subsection.4.3.3}% 
\contentsline {chapter}{\numberline {5}Instrukcja u\IeC {\.z}ytkowania}{17}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Instalacja aplikacji}{17}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Obs\IeC {\l }uga aplikacji}{17}{section.5.2}% 
\contentsline {section}{\numberline {5.3}Przeprowadzenie treningu}{19}{section.5.3}% 
\contentsline {chapter}{\numberline {6}Wyniki}{21}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Testy treningu sieci neuronowych}{21}{section.6.1}% 
\contentsline {subsection}{\numberline {6.1.1}Testy architektur}{21}{subsection.6.1.1}% 
\contentsline {subsection}{\numberline {6.1.2}Testy precyzji szesnastobitowej}{22}{subsection.6.1.2}% 
\contentsline {section}{\numberline {6.2}Wp\IeC {\l }yw modyfikacji danych na dok\IeC {\l }adno\IeC {\'s}\IeC {\'c} sieci}{22}{section.6.2}% 
\contentsline {subsection}{\numberline {6.2.1}Jasno\IeC {\'s}\IeC {\'c}}{23}{subsection.6.2.1}% 
\contentsline {subsection}{\numberline {6.2.2}Kontrast}{24}{subsection.6.2.2}% 
\contentsline {subsection}{\numberline {6.2.3}Zbli\IeC {\.z}enia}{25}{subsection.6.2.3}% 
\contentsline {subsection}{\numberline {6.2.4}Losowe zbli\IeC {\.z}enia}{26}{subsection.6.2.4}% 
\contentsline {subsection}{\numberline {6.2.5}Rotacje}{27}{subsection.6.2.5}% 
\contentsline {subsection}{\numberline {6.2.6}Kompresja JPEG}{28}{subsection.6.2.6}% 
\contentsline {subsection}{\numberline {6.2.7}Wypaczenie perspektywy}{29}{subsection.6.2.7}% 
\contentsline {subsection}{\numberline {6.2.8}Podsumowanie manipulacji danych}{30}{subsection.6.2.8}% 
\contentsline {section}{\numberline {6.3}Selektywna wsteczna propagacja}{30}{section.6.3}% 
\contentsline {section}{\numberline {6.4}Miejsca uwagi sieci neuronowej}{30}{section.6.4}% 
\contentsline {section}{\numberline {6.5}Rezultaty finalnego treningu}{31}{section.6.5}% 
\contentsline {chapter}{\numberline {7}Podsumowanie}{33}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Wnioski}{33}{section.7.1}% 
\contentsline {section}{\numberline {7.2}Opis ogranicze\IeC {\'n} algorytmu}{33}{section.7.2}% 
\contentsline {section}{\numberline {7.3}Mo\IeC {\.z}liwe modyfikacje}{33}{section.7.3}% 
\contentsline {chapter}{Bibliografia}{36}{chapter*.38}% 
\contentsline {chapter}{\numberline {A}Zawarto\IeC {\'s}\IeC {\'c} p\IeC {\l }yty CD}{37}{appendix.A}% 
